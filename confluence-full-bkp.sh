#!/bin/bash
#Script aimed to backup Confluence application on a file system level.
#Backup subjects is Installation directory(beforehand cleared log directory)
#Home directory with(beforehand cleared backups and log directories)

dir=$(date +%d-%m-%Y)-Confluence
mkdir -p $dir/logs
cd $dir
/etc/init.d/confluence stop 2>>logs/log.txt
find /opt/atlassian/confluence/logs/  -mtime +7 -type f -exec rm {} \; 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: installation log directory cleared" >>logs/log.txt
fi
tar cvfP confluence-install.tar /opt/atlassian/confluence 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: Confluence installation directory backed up" >>logs/log.txt
fi
find /var/atlassian/application-data/confluence/backups/ -mtime +7 -type f -exec rm {} \; 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: backups directory cleared" >>logs/log.txt
fi
find /var/atlassian/application-data/confluence/logs/ -mtime +7 -type f -exec rm {} \; 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: Confluence home directory log files cleared" >>logs/log.txt
fi
tar cvfP confluence-home.tar /var/atlassian/application-data/confluence 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful Jira home directory backed up" >>logs/log.txt
fi
/etc/init.d/confluence start 2>>logs/log.txt
n=$(grep -o 'Successful' logs/log.txt |wc -l)
echo $n
let prco=5-$n
if [ $n -eq 5 ]
then
  echo "NO PROBLEM DETECTED" >>logs/log.txt
else
  echo "$prco PROBLEM DETECTED" >>logs/log.txt
fi
