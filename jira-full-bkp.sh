#!/bin/bash
#Script aimed to backup Jira application on a file system level.
#Backup subjects is Installation directory(beforehand cleared log directory)
#Home directory with(beforehand cleared export and log directories)

dir=$(date +%d-%m-%Y)-Jira
mkdir -p $dir/logs
cd $dir
/etc/init.d/jira stop 2>>logs/log.txt
find /opt/atlassian/jira/logs/  -mtime +7 -type f -exec rm {} \; 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: installation log directory cleared" >>logs/log.txt
fi
tar cvfP jira-install.tar /opt/atlassian/jira 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: Jira installation directory backed up" >>logs/log.txt
fi
find /var/atlassian/application-data/jira/export/ -mtime +7 -type f -exec rm {} \; 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: export directory cleared" >>logs/log.txt
fi
find /var/atlassian/application-data/jira/log -mtime +7 -type f -exec rm {} \; 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful: home directory log files cleared" >>logs/log.txt
fi
tar cvfP jira-home.tar /var/atlassian/application-data/jira 2>>logs/log.txt
if [ $? -eq 0 ]
then
  echo "$(date) Successful Jira home directory backed up" >>logs/log.txt
fi
/etc/init.d/jira start 2>>logs/log.txt
n=$(grep -o 'Successful' logs/log.txt |wc -l)
echo $n
let prco=5-$n
if [ $n -eq 5 ]
then
  echo "NO PROBLEM DETECTED" >>logs/log.txt
else
  echo "$prco PROBLEM DETECTED" >>logs/log.txt
fi
